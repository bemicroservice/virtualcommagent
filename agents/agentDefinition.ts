

/**
 * Communication agent from configuration. 
 */
export interface CommAgent{
  type: "http" | "redis" | "rabbitmq" | "websocket",
  definition: HttpAgentConfig
}

/**
 * Http agent definition
 */
export interface HttpAgentConfig{
  url: string,
  method: "get" | "post" | "put" | "patch" | "delete" | "head",
  headers?: any,
  body?: any,
  interval: number
  assert: {
    statusCode: number,
    body?: string,
    bodyCondition?: "equal" | "includes"
  }
}