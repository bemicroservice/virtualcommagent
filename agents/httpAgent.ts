import axios from "axios";
import { HttpAgentConfig } from "./agentDefinition";

export default class HttpAgent {
  constructor(public agentDefinition: HttpAgentConfig) { }

  runAgent = () => {
    const { url, method, interval } = this.agentDefinition
    return setInterval(async () => {
      try {
        const { status, statusText, data } = await this.executeHttpRequest()
        const message = `${method}: ${url} ${status} ${statusText}`
        console.log(message)
        this.assertResponse(status, data)
      } catch (error) {
        console.warn(`Error to ${url}`)
      }
    }, interval)
  }

  private executeHttpRequest = async () => {

    const { url, method, headers, body } = this.agentDefinition
    const { get, post, put, patch, head, delete: httpDelete, ...rest } = axios
    switch (method) {
      case "get":
        return axios.get(url, { headers })
      case "post":
        return axios.post(url, body, { headers })
      case "put":
        return axios.put(url, body, { headers })
      case "patch":
        return axios.patch(url, body, { headers })
      case "delete":
        return axios.delete(url, { headers })
      case "head":
        return axios.head(url,{ headers })

      default:
        throw new Error("Invalid method in config");
    }
  }

  assertResponse = (recivedStatusCode: number, recivedBody?: string) => {

    const { statusCode, body, bodyCondition } = this.agentDefinition.assert

    if (recivedStatusCode !== statusCode)
      console.warn(`Expected status ${statusCode} do not equal result ${recivedStatusCode}`)

    if (body && bodyCondition) {

      if (!recivedBody || recivedBody === "")
        console.warn(`Expected body, but there is nothing in response`)

      if (bodyCondition === "equal" && body !== recivedBody)
        console.warn(`Body "${recivedBody} do not equal "${body}"`)

      if (bodyCondition === "includes" && !recivedBody?.includes(body))
        console.warn(`Body "${recivedBody} do not include "${body}"`)
    }
  }
}


