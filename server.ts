import http from "http";
import HttpAgent from "./agents/httpAgent"
import { fromJsonFile } from "./utils/configReader";

const PORT = process.env.PORT || 8000
const CONFIG_PATH = process.env.CONFIG_PATH || "config/request.json"

//load agent configuration from JSON 
const agentDefinitions = fromJsonFile(CONFIG_PATH)

//select HTTP agents (others comming soon)
const httpAgentsDefinitions = agentDefinitions.filter(agent => agent.type === "http")

//start basic node server
http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/plain' });
  res.end('Server OK');
}).listen(PORT).once("listening", () => console.log("Server listening"))


//run all agents
httpAgentsDefinitions.forEach(commAgent => new HttpAgent(commAgent.definition).runAgent())