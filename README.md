# Communication Agent
Simulate virtual user / service, that send communication requests in loop by configuration file. Also assert response data agains expected values.

## Where to use this service?
You have a microservice where requests triggers some environment action (MQ events etc.) This dockerized agent simulate user / service communication, so you can configure, test and monitor your environment (tools, services etc.).

## Configuration
Done by `JSON` file folder. Check `schema.json` and `agents/agentDefinition` files to see configuration schema.

Default configuration file path is `config/request.json`,, but can be set up by ENV variable.

See `docker-compose.yml` for list of environment variables and volume setup.

### JSON file sample
`[
  {
    "type": "http",
    "definition": {
      "url": "https://www.google.com",
      "headers": [],
      "method": "get",
      "interval": 1000,
      "assert":{
        "statusCode": 200,
        "body": "a",
        "bodyCondition": "includes"
      }
    }
  }
]`

## Supported APIs
- Http (GET, POST, PATCH, DELETE, HEAD) with headers and body
- Websockets (soon...)
- Redis Pubsub (soon...)
- RabbitMQ (soon...)

## Install
1. Install Node.js and NPM
2. Clone the repository
3. Inside, create `config` folder with `request.json` (default path)
4. Define requests inside the JSON file.
5. Run `npm run dev` for development mode.
6. Update `docker-compose.yml` by your requirements.
6. Run `docker-compose up --build` for docker mode.