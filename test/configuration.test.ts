import { expect } from "chai"
import { CommAgent } from "../agents/agentDefinition"
import { fromJSON, fromJsonFile } from "../utils/configReader"

const jsonStringCorrect = `
[
  {
    "type": "http",
    "definition": {
      "url": "https://www.w3schools.com/nodejs/",
      "headers": [],
      "method": "get",
      "interval": 1000,
      "assert":{
        "status": 200,
        "body": "test",
        "bodyCondition": "equal"
      }
    }
  }
]
`

const expectBlock = (result: CommAgent[]) => {
  const firstAgent = result[0]

  expect(result).haveOwnProperty("length")
  expect(firstAgent).haveOwnProperty("type", "http")
  expect(firstAgent).haveOwnProperty("type", "http")
  expect(firstAgent).haveOwnProperty("definition")

  const firstAgentDefinition = firstAgent.definition
  expect(firstAgentDefinition).haveOwnProperty("url").equal("https://www.w3schools.com/nodejs/")
  expect(firstAgentDefinition).haveOwnProperty("headers")
  expect(firstAgentDefinition).haveOwnProperty("method").equals("get")
  expect(firstAgentDefinition).haveOwnProperty("interval").equals(1000)

  const firstAgentDefinitionAssert = firstAgent.definition.assert
  expect(firstAgentDefinitionAssert).haveOwnProperty("status").equal(200)
  expect(firstAgentDefinitionAssert).haveOwnProperty("body").equals("test")
  expect(firstAgentDefinitionAssert).haveOwnProperty("bodyCondition").equals("equal")
}


describe("Testing cofiguration upload", () => {

  it("Test HTTP from string", () => {
    const result: CommAgent[] = fromJSON(jsonStringCorrect)
    expectBlock(result)
  }) 

  it("Test HTTP from file", () => {
    const result: CommAgent[] = fromJsonFile("test/fixtures/test-config.json")
    expectBlock(result)
  }) 
})