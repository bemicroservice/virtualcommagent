import { expect } from "chai"
import { CommAgent } from "../agents/agentDefinition"
import HttpAgent from "../agents/httpAgent"
import { fromJsonFile } from "../utils/configReader"

describe("Test configuration of http agent", () => {

  it("Test Http agent", () => {
    const config: CommAgent[] = fromJsonFile("test/fixtures/test-config.json")
    const agent = new HttpAgent(config[0].definition)
  
    const { agentDefinition}  = agent
    expect(agentDefinition).haveOwnProperty("url").equal("https://www.w3schools.com/nodejs/")
    expect(agentDefinition).haveOwnProperty("headers")
    expect(agentDefinition).haveOwnProperty("method").equals("get")
    expect(agentDefinition).haveOwnProperty("interval").equals(1000)
  
    const {assert: assertionDef} = agentDefinition
    expect(assertionDef).haveOwnProperty("status").equal(200)
    expect(assertionDef).haveOwnProperty("body").equals("test")
    expect(assertionDef).haveOwnProperty("bodyCondition").equals("equal")
  })

})