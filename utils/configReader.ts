import filesystem from "fs";
import { CommAgent, HttpAgentConfig } from "../agents/agentDefinition";


/**
 * Read agents from stringified JSON configuration  content
 * @param jsonContent JSON string content
 * @returns List of all agents
 */
export const fromJSON = (jsonContent: string): Array<CommAgent> => {

  const agentsConfig = JSON.parse(jsonContent.toString()) as any[]

  if (jsonContent.length === 0)
    throw new Error("No Agents Defined");

  return agentsConfig.map<CommAgent>(record => {

    const recordType = record["type"]

    if (recordType == "http") {
      const definition = record["definition"]
      return {
        type: "http",
        definition: <HttpAgentConfig>definition
      }
    }

    else throw new Error("Unsupported agent type");

  })
}

/**
 * Load agents from existing JSON file
 * @param path Path to the JSON file
 * @returns List of all agents
 */
export const fromJsonFile = (path: string) => {
  const dataBuffer = filesystem.readFileSync(path)
  return fromJSON(dataBuffer.toString())
}
